/**
 * @brief OTA 功能实现以及联网下载功能、校验之类的
 * @param	void
 * @retval void
 * @author ZCD1300 
 * @Time 2022年3月25日
*/
#include "OTA.h"
#include "IAP.h"

// uint16_t Package_NUM=0;
// uint16_t Package_total=0;

uint8_t OTAIAPDownloadStart=0;
uint8_t ForceUpdateEnableFlag=0;
uint8_t AutoDownloadFlag=0;
/**
 * @brief OTA 流程控制函数(默认检查最新的固件)
 * @param	void
 * @retval int8_t return 0-正常； 1-拉取固件信息失败; 2-无网络连接（会自动重连，再手动重试升级）; 固件无需升级
 * @author ZCD1300 
 * @Time 2022年4月13日
*/
int8_t OTA_Ctrl(void)
{
	TIM_IT_Ctrl(&htim3,0);//关定时器中断方式升级流程被打断

	if(TCPLink_Alive==1)
	{//TCP连接存在
		//Page_Switch(0);//屏幕切换到page0
		if(	GetFiremwareINFOLest_OTA(1,1)==0)
		{//拉取固件信息成功
		
			{//拉取的固件信息转存到结构体
				strcpy(FirmwareINFO.FirmwareType,"SNHOST-1");
				FirmwareINFO.FirmwareVersion[0]='Y';
				FirmwareINFO.FirmwareVersion[1]=LatestFirmware_Ver.Str[0];
				FirmwareINFO.FirmwareVersion[2]=LatestFirmware_Ver.Str[2];
				FirmwareINFO.FirmwareVersion[3]=LatestFirmware_Ver.Str[4];
				for(uint8_t i=0;i<32;i++)
				{
					FirmwareINFO.Firmware_MD5[i]=LatestFirmware_MD5.Str[i];
				}
				FirmwareINFO.FirmwareSize=LatestFirmware_Size;
			}
			{//判断固件升级条件
				uint16_t NewVerCmp_temp=0;
				uint16_t LocalVerCmp_temp=0;
				LocalVerCmp_temp= ((FirmwareINFO_Local.FirmwareVersion[1]-48)*100) +\
													((FirmwareINFO_Local.FirmwareVersion[2]-48)*10) +\
													( FirmwareINFO_Local.FirmwareVersion[3]-48);//本地固件代号
				NewVerCmp_temp =	((FirmwareINFO.FirmwareVersion[1]-48)*100) +\
													((FirmwareINFO.FirmwareVersion[2]-48)*10) +\
													( FirmwareINFO.FirmwareVersion[3]-48);//新固件固件代号

				if (NewVerCmp_temp>LocalVerCmp_temp)
				{//服务器固件版本号更高，可升级

					{
						//这里预留强制升级部分的代码
						if(AutoDownloadFlag==1)
						{
							KeyState[0]=0;//自动下载
							if(FirmwareINFO.FirmwareVersion[3]=='0')//版本标号最后一位为0强制升级
							{
								ForceUpdateEnableFlag=1;
							}
							
						}
					}
					{//询问是否升级（YES/NO）
						DebugPrint_uart_LineFeed("New firmware is available for download. Do you want to start downloading?(YES/NO)",200);
						UART2_Refresh_Flag=0;
						if(Wait_Until_FlagTrue(&UART2_Refresh_Flag,0)==1||KeyState[0]==0)//固件下载确认(手动输入或者按键确认)
						{
							if(Uart_Debug_CMD_ADD("YES",Rx_Buff2,0,5)||KeyState[0]==0)
							{
								Page_Switch(2);//屏幕切换到page2
								KeyState[0]=1;
								AutoDownloadFlag=0;
								DebugPrint_uart_LineFeed("Downloading...",200);
								DebugPrint_uart_LineFeed("** 0%",200);
								LCD_FirmwareDownload_ProgressBar(0);
								OTA_FirmwareDownload(IAP_packageBuff,1024,FirmwareINFO);								
								if(FirmwareMD5HashCheck(FirmwareDownloadAreaFlash_StartPtr+FirmwareINFO_Offset)==0)
								{//MD5校验通过
									if(ForceUpdateEnableFlag!=1)
									{
										BootloaderIAP_ParamWrite(2);//下载完成不立即升级
										DebugPrint_uart_LineFeed("MD5 check correct.",200);
									}
									else
									{
										BootloaderIAP_ParamWrite(1);//下载完后立即升级
										DebugPrint_uart_LineFeed("MD5 check correct.Automatically start the upgrade and the system will restart automatically.",200);
										Delay(1000);
										MCU_System_RST();
									}
								
								}
								else
								{
									BootloaderIAP_ParamWrite(0);//MD5校验错误，取消升级	
									DebugPrint_uart_LineFeed("MD5 check error.",200);//MD5校验失败
								}
							}
							else
							{//固件下载取消

							}
							Page_Switch(1);//屏幕切换到page1
							page1();
							TIM_IT_Ctrl(&htim3,1);//重开定时器中断
							return 0;
						}
					}
				}
				else
				{//当前本地版本无需升级
					DebugPrint_uart_LineFeed("The current version does not need to be upgraded.",200);//
					Page_Switch(1);//屏幕切换到page1
					page1();
					TIM_IT_Ctrl(&htim3,1);//重开定时器中断
					return 3;//固件无需升级
				}
			}
		}
		else
		{//固件信息拉取失败
			Page_Switch(1);//屏幕切换到page1
			page1();
			TIM_IT_Ctrl(&htim3,1);//重开定时器中断
			return 1;
		}
	}
	else
	{//连接断开
		Page_Switch(1);//屏幕切换到page1	
		page1();
		TIM_IT_Ctrl(&htim3,1);//重开定时器中断
		SmartNest_Main_Function();//借用数据上传函数重建连接
		DebugPrint_uart_LineFeed("The network connection is disconnected. Please try the upgrade again later.",200);
		return 2;
	}
	Page_Switch(1);//屏幕切换到page1
	page1();
	TIM_IT_Ctrl(&htim3,1);//重开定时器中断
}
/**
 * @brief OTA 固件下载函数
 * @param	void
 * @retval int8_t return 0-正常； 1-固件无效; 2-无网络连接（会自动重连，再手动重试升级）; 固件无需升级
 * @author ZCD1300 
 * @Time 2022年4月13日
*/
int8_t OTA_FirmwareDownload(uint8_t * packageBuff_t,uint16_t SinglePackSize_t,FirmwareINFO_t TargetFirmwareINFO_t)
{
	if(OTAIAPDownloadStart==0)//下载前准备 
	{
		//清空下载存储用的Flash空间
		EraseDownloadAreaFlash();
		if(Wirte_firmwareINFO(TargetFirmwareINFO_t)!=0)//固件有效、复写固件信息
		{
			return 1;//固件信息写入失败
		}		
		PackageTotal=TargetFirmwareINFO_t.FirmwareSize/SinglePackSize_t;//计算一个固件需要需要分多少包
		if (PackageTotal*SinglePackSize_t< TargetFirmwareINFO_t.FirmwareSize)
		{
			PackageTotal++;
			LastPackNotFull=1;//最后一个包大小不完整
		}
		else
		{
			LastPackNotFull=0;
		}			
		OTAIAPDownloadStart=1;
	}
	uint16_t OTA_DownloadPercentageprogressTotalVal=PackageTotal;
	while (PackageTotal)
	{
		if(GetFiremwareBIN_OTA(FlashOperatePtr-(FirmwareDownloadAreaFlash_StartPtr+FirmwareINFO_Offset),FirmwareINFO.FirmwareVersion)==0)
		{
			packageBuffRefreshedFLAG=1;
			IAP_Ctrl(packageBuff_t,SinglePackSize_t,TargetFirmwareINFO_t);
			LCD_FirmwareDownload_ProgressBar((100*(OTA_DownloadPercentageprogressTotalVal-PackageTotal))/OTA_DownloadPercentageprogressTotalVal);
			UART_Print_Percentage_progress(OTA_DownloadPercentageprogressTotalVal,OTA_DownloadPercentageprogressTotalVal-PackageTotal);
		}
		else
		{//固件下载出错
			DebugPrint_uart_LineFeed("An error occurred during firmware download.",200);
		
		}
	}

	FlashOperatePtr=FirmwareDownloadAreaFlash_StartPtr+FirmwareINFO_Offset;
	OTAIAPDownloadStart=0;
	TIM_IT_Ctrl(&htim3,1);//重新打开定时器中断
	DebugPrint_uart_LineFeed("Download firmware via Network finished. Next, perform MD5 check.",200);
	DebugPrint_uart_LineFeed("Calculating MD5...",200);
	return 0;
	

}

/**
 * @brief OTA 固件升级自动检测（如果检测到固件最后一位固件号是0，则执行强制升级）
 * @param	void
 * @retval int8_t return 0-正常； 1-固件无效; 2-无网络连接（会自动重连，再手动重试升级）; 固件无需升级
 * @author ZCD1300 
 * @Time 2022年4月24日
*/
int8_t OTA_FirmwareAutoCheck(void)
{
	//判断升级时间
	if(hour>0&&hour<3)
	{//凌晨0~3点可以升级
		//升级检查
		ForceUpdateEnableFlag=0;
		AutoDownloadFlag=1;
		OTA_Ctrl();
		
	}
	else
	{return 1;}
}

