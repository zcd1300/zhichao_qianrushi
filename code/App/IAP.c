/**
 * @brief IAP 功能实现，此方案是IAP下载，flash下载操作在主APP中
 * @param	void
 * @retval void
 * @author ZCD1300 
 * @Time 2022年3月25日
*/
#include "IAP.h"
#if defined HostAPP
	#include "OTA.h"
#endif

uint8_t IAP_packageBuff[1024+PackageAddtINFO_Size]={0};//存放分包发送过来的Bin文件，大小是1K+PackageAddtINFO_Size字节的其他信息以及裕量
uint32_t FlashOperatePtr=FirmwareDownloadAreaFlash_StartPtr+FirmwareINFO_Offset;//Flash操作过程中的指针，指向固件进度的最高位
FirmwareINFO_t FirmwareINFO={0};//固件信息存储结构体



/**
 * @brief 写内存中的固件数据到Flash
 * @param	*packageBuff_t	buffInfoSize_t	数据地址指针、数据大小(单位 字节 ，实际写入转换成字-32位)
 * @retval void
 * @author ZCD1300 
 * @Time 2022年3月25日
 * @WatchOut @ 
			       @ 此函数可对OTP区操作（只能写入0，不能擦除）
			       @ WriteAddr必须是4的倍数,其实写入地址和写入后的指针也要是4的倍数
*/
uint8_t Write_FirmwarePackageBuff_To_Flash(uint8_t *packageBuff_t,uint32_t buffInfoSize_t)
{
	uint32_t ByteToWord_CNT=0;//实际写入的字数，Flash只能按字写入；
	ByteToWord_CNT=buffInfoSize_t/4;
	if(buffInfoSize_t%4==0)
	{
		Flash_Write(FlashOperatePtr,ByteToWord_CNT,(uint32_t *)IAP_packageBuff);
		FlashOperatePtr+=ByteToWord_CNT*4;		
	}
	else
	{
		return 1;
//		ByteToWord_CNT=ByteToWord_CNT+1;
//		Flash_Write(FlashOperatePtr,ByteToWord_CNT,(uint32_t *)IAP_packageBuff);
//		FlashOperatePtr+=ByteToWord_CNT;			
	}

	return 0;
}
/**
 * @brief 读取Flash中的固件数据到内存
 * @param :FirmwareStartAddr, Read_Len_t, *RAM_pBuff ；固件开始读取的存储地址，读取长度，读取后存储地址
 * @retval uint8_t 读取结果 1-读取错误 0-正常读取 2-没有有效的固件信息
 * @author ZCD1300 
 * @Time 2022年3月26日
*/
uint8_t Read_FirmwarePackage_To_RAMBuff(uint32_t FirmwareStartAddr,uint32_t Read_Len_t,uint32_t * RAM_pBuff)
{
	if(FirmwareINFO.FirmwareSize<=0)
	{//如果固件不存在、或者是没有更新固件信息
		return 2;
	}
	if(Read_Len_t>FirmwareINFO.FirmwareSize)//读取长度超过固件大小
	{//读取长度超过固件大小
		return 1;
	}
	else 
	{
		Flash_Read(FirmwareStartAddr,Read_Len_t,RAM_pBuff);
		return 0; 
	}
}
/**
 * @brief 更新（读取）固件信息，通过读取指定区域Flash内容更新Firmware信息
 * @param void
 * @retval uint8_t 读取结果 1-下载区无有效固件 0-正常读取 2-意外错误
 * @author ZCD1300 
 * @Time 2022年3月27日
*/
uint8_t Update_FirmwareINFO(void)
{
	uint8_t FirmwareINFO_RAW[FirmwareINFO_Offset]={0};
	Flash_Read(FirmwareDownloadAreaFlash_StartPtr,FirmwareINFO_Size,(uint32_t *)FirmwareINFO_RAW);
	if (FirmwareINFO_RAW[8]=='Y')
	{
		/* 存在有效固件 */
		memcpy(FirmwareINFO.FirmwareType,FirmwareINFO_RAW,4*FirmwareINFO_Size);
		return 0;
	}
	else if (FirmwareINFO_RAW[8]=='N')
	{
		/* 无有效固件 */
		return 1;
	}
	else
	{
		/* 无有效固件 */
		return 1;
	}
	return 2;
}
/**
 * @brief 复写（写入）固件信息，通过擦除指定的整个区块Flash内容,重新写入Firmware信息,此功能应该在开始下载固件之前完成
 * @param void
 * @retval uint8_t 读取结果 1-固件信息无效 0-正常读取 2-数据大小错误
 * @author ZCD1300 
 * @Time 2022年3月27日
*/
uint8_t Wirte_firmwareINFO(FirmwareINFO_t NewFirmwareINFO_t)
{
	
	if((FirmwareINFO_Size<4)||(FirmwareINFO_Size>FirmwareINFO_Offset))
	{
		return 2;
	}
	if(NewFirmwareINFO_t.FirmwareVersion[0]=='Y')
	{
		Flash_Write(FirmwareDownloadAreaFlash_StartPtr,FirmwareINFO_Size,(uint32_t *)&NewFirmwareINFO_t);
		return 0;
	}
	else
	{
		return 1;//固件信息无效
	}

}

/**
 * @brief Erase download area flash sector (ADDR_Flash_Sector_7)
 * @param void 
 * @retval  void
 * @author ZCD1300 
 * @Time 2022年4月9日
*/
void EraseDownloadAreaFlash(void)
{
	Flash_Erase_Sector(FLASH_SECTOR_7);
	
}
/**
 * @brief Erase APP1 area flash sector （start：0x08008000）
 * @param void 
 * @retval  void
 * @author ZCD1300 
 * @Time 2022年4月14日
*/
void EraseAPP1AreaFlash(void)
{
	Flash_Erase_Sector(FLASH_SECTOR_2);
	Flash_Erase_Sector(FLASH_SECTOR_3);
	Flash_Erase_Sector(FLASH_SECTOR_4);
	Flash_Erase_Sector(FLASH_SECTOR_5);
	Flash_Erase_Sector(FLASH_SECTOR_6);	
}

uint8_t packageBuffRefreshedFLAG=0;//下载缓存被刷新标志
uint8_t USBUpdateTriggerFlag=0;
uint32_t PackageTotal=0;
uint8_t LastPackNotFull=0;
/**
 * @brief IAP 下载流程控制函数,调用此函数完成固件下载
 * @param  uint8_t* 下载缓存区指针,uint32_t 单个包大小,FirmwareINFO_t 固件信息结构体指针
 * @retval  读取结果 1-复写固件信息失败 0-正常读取 2-没有有效的固件信息
 * @author ZCD1300 
 * @Time 2022年3月30日
*/
uint8_t IAP_Ctrl(uint8_t * packageBuff_t,uint32_t SinglePackSize_t,FirmwareINFO_t TargetFirmwareINFO_t)
{
	
	if(PackageTotal)
	{
		/* code */
		if(packageBuffRefreshedFLAG)
		{//下载缓存区被新数据刷新过
			
			if ( LastPackNotFull == 0 )
			{
				/* code */
				Write_FirmwarePackageBuff_To_Flash(packageBuff_t,SinglePackSize_t);
			}
			else
			{
				if (PackageTotal==1)
				{//最后一包,不完整
					/* code */
					uint32_t lastpackSize=TargetFirmwareINFO_t.FirmwareSize%SinglePackSize_t;
					uint8_t lastpackSizeLess4_temp = lastpackSize%4;
					//Write_FirmwarePackageBuff_To_Flash(packageBuff_t,lastpackSize);
					if(lastpackSizeLess4_temp==0)
					{
						Write_FirmwarePackageBuff_To_Flash(packageBuff_t,lastpackSize);
					}
					else
					{
						for(uint8_t i=0;i<(4-lastpackSizeLess4_temp);i++)
						{
							IAP_packageBuff[lastpackSize+i]=0xff;
						}
						Write_FirmwarePackageBuff_To_Flash(packageBuff_t,lastpackSize+(4-lastpackSizeLess4_temp));
					}
				}
				else
				{
					Write_FirmwarePackageBuff_To_Flash(packageBuff_t,SinglePackSize_t);
				}
			}
			packageBuffRefreshedFLAG=0;
			PackageTotal--;
		}
	}
	return 0;

}
#if defined LWIP_INCLUDED_POLARSSL_MD5_H
uint8_t MD5HashLoaclCal[32]={0};
/**
 * @brief Firmware MD5 hash check
 * @param uint32_t flash_addr 下载区域的flash地址
 * @retval uint8_t 校验结果 0-校验相同  1-校验失败
 * @author ZCD1300 
 * @Time 2022年4月9日
*/
uint8_t FirmwareMD5HashCheck(uint32_t DownloadAreaFlashADDR_t)
{
	uint32_t FirmwareADDRPtr=DownloadAreaFlashADDR_t;
	uint8_t MD5LocalOutput_t[16]={0}; 
	uint16_t HashPackageNUM=0; 
	uint32_t LessThan1024Leth=0;

	HashPackageNUM=FirmwareINFO.FirmwareSize/1024;
	if((FirmwareINFO.FirmwareSize%1024)!=0)
	{
		HashPackageNUM++;
	}
	
	md5_context ctx_usb;
	md5_starts( &ctx_usb);
	for(uint16_t i=0;i<HashPackageNUM;i++)
	{
		if(i==(HashPackageNUM-1))
		{
			LessThan1024Leth=FirmwareINFO.FirmwareSize%1024;
			if((LessThan1024Leth%4)!=0)
			{
				LessThan1024Leth=(LessThan1024Leth/4)+1;
			}
			else
			{LessThan1024Leth=LessThan1024Leth/4;}
			Read_FirmwarePackage_To_RAMBuff(FirmwareADDRPtr,LessThan1024Leth,(uint32_t*)IAP_packageBuff);
			FirmwareADDRPtr+=(FirmwareINFO.FirmwareSize%1024);			
			md5_update( &ctx_usb,IAP_packageBuff, FirmwareINFO.FirmwareSize%1024 );
		}
		else
		{

			Read_FirmwarePackage_To_RAMBuff(FirmwareADDRPtr,1024/4,(uint32_t*)IAP_packageBuff);
			FirmwareADDRPtr+=1024;
			md5_update( &ctx_usb,IAP_packageBuff, 1024 );
		}
	}

	md5_finish(&ctx_usb,MD5LocalOutput_t);
	for(uint8_t n=0;n<16;n++)
	{//MD5
		uint8TransToHEXchar(MD5LocalOutput_t[n],&MD5HashLoaclCal[2*n],0);
	}	

	uint8_t MD5CMP_cnt=0;
	for(uint8_t j=0;j<32;j++)
	{
		if(MD5HashLoaclCal[j]==FirmwareINFO.Firmware_MD5[j])
		{
			MD5CMP_cnt++;
		}
	}
	if(MD5CMP_cnt==32)
	{return 0;}//MD5校验相同
	else
	{return 1;}//MD5校验失败
}
#endif

#if defined __USB_HID_Z_
#if defined HostAPP
#include "ESP8266.h"
uint8_t USBIAPDownloadStart=0;
/**
 * @brief IAP via USB HID
 * @param  uint8_t * packageBuff_t(下载缓存区),uint32_t SinglePackSize_t（单个包大小）,FirmwareINFO_t TargetFirmwareINFO_t（待升级固件信息）
 * @retval  读取结果 1-复写固件信息失败 0-正常读取 2-没有有效的固件信息
 * @author ZCD1300 
 * @Time 2022年4月5日
*/
uint8_t IAP_via_USB(uint8_t * packageBuff_t,uint32_t SinglePackSize_t,FirmwareINFO_t TargetFirmwareINFO_t)
{
	if (TargetFirmwareINFO_t.FirmwareVersion[0]!='Y')//先判断固件是否有效
	{
		/* code */
		return 2;
	}	
	if(USBIAPDownloadStart==0)//下载前准备
	{
		//清空下载存储用的Flash空间
		EraseDownloadAreaFlash();
		if(Wirte_firmwareINFO(TargetFirmwareINFO_t)!=0)//固件有效、复写固件信息
		{
			return 1;
		}	
		PackageTotal=TargetFirmwareINFO_t.FirmwareSize/SinglePackSize_t;//计算一个固件需要需要分多少包
		if (PackageTotal*SinglePackSize_t< TargetFirmwareINFO_t.FirmwareSize)
		{
			PackageTotal++;
			LastPackNotFull=1;//最后一个包大小不完整
		}
		else
		{
			LastPackNotFull=0;
		}
		USBIAPDownloadStart=1;//开始下载标志置位
		TIM_IT_Ctrl(&htim3,0);//临时关定时器中断
	}
	for(uint16_t buffTrans_t=0;buffTrans_t<SinglePackSize_t;buffTrans_t++)
	{
		IAP_packageBuff[buffTrans_t] = packageBuff_t[buffTrans_t];

	}
	IAP_Ctrl(packageBuff_t,SinglePackSize_t,TargetFirmwareINFO_t);
	if(PackageTotal==0)
	{
		FlashOperatePtr=FirmwareDownloadAreaFlash_StartPtr+FirmwareINFO_Offset;
		USBIAPDownloadStart=0;//开始下载标志复位
		USBUpdateTriggerFlag=0;//复位升级触发标志
		TIM_IT_Ctrl(&htim3,1);//重新打开定时器中断
		return 0;
	}	
}

/**
 * @brief USB IAP firmware info receive
 * @param void
 * @retval  读取结果 1-复写固件信息失败 0-正常读取
 * @author ZCD1300 
 * @Time 2022年4月8日
*/
uint8_t IAP_USB_ReceiveFirmwareINFO(void)
{
	uint32_t INFOInputStartPtr_t=0;
	uint32_t INFOInputEndPtr_t=0;
	//uint32_t INFOInputTypePtr_t=0;
	uint32_t INFOInputVerPtr_t=0;
	uint32_t INFOInputSizePtr_t=0;
	uint32_t INFOInputMD5Ptr_t=0;	
	uint8_t FirmwareInfoTempbuff_USB[60]={0};
	while (1)
	{
		if(StringSearch_Assign("FirmwareINFO:{",Rx_Buff2,14,100))
		{
			INFOInputStartPtr_t=StringSecanPointer_Assign_Buff+14;
			if(StringSearch_Assign("}END",Rx_Buff2,4,100))
			{
				INFOInputEndPtr_t=StringSecanPointer_Assign_Buff;
			}
			else
			{
				INFOInputStartPtr_t=0;
				INFOInputEndPtr_t=0;
				continue;
			}
		}
		else
		{
			continue;
		}

		if ((INFOInputEndPtr_t>INFOInputStartPtr_t)&&((INFOInputEndPtr_t-INFOInputStartPtr_t)<=60))//60是输入最大的INFO长度
		{//输入的固件信息有效
		 //FirmwareINFO:{12345678,Y123,1024000,MD5:1234567890123456}END  固件信息格式，建议转换位16进制发送
			for(uint8_t i=0;i<(INFOInputEndPtr_t-INFOInputStartPtr_t);i++)
			{
				FirmwareInfoTempbuff_USB[i]=Rx_Buff2[INFOInputStartPtr_t+i];
			}
			uint8_t INFODataTypeCheckCnt_t=0;
			if(StringSearch_Assign(",",FirmwareInfoTempbuff_USB,1,60))
			{
				INFODataTypeCheckCnt_t++;
				FirmwareInfoTempbuff_USB[StringSecanPointer_Assign_Buff]=0;
				INFOInputVerPtr_t=StringSecanPointer_Assign_Buff+1;
			}

			if(StringSearch_Assign(",",FirmwareInfoTempbuff_USB,1,60))
			{
				INFODataTypeCheckCnt_t++;
				FirmwareInfoTempbuff_USB[StringSecanPointer_Assign_Buff]=0;
				INFOInputSizePtr_t=StringSecanPointer_Assign_Buff+1;
			}

			if(StringSearch_Assign("MD5:",FirmwareInfoTempbuff_USB,4,60))
			{
				INFODataTypeCheckCnt_t++;
				FirmwareInfoTempbuff_USB[StringSecanPointer_Assign_Buff]=0;		
				INFOInputMD5Ptr_t=StringSecanPointer_Assign_Buff+4;
			}

			if(INFODataTypeCheckCnt_t==3)
			{//固件全部信息定位完成，写入结构体
				for(uint8_t j=0;j<(INFOInputVerPtr_t-1);j++)
				{//TYPE
					FirmwareINFO.FirmwareType[j]=FirmwareInfoTempbuff_USB[j];
				}	
				for(uint8_t k=0;k<4;k++)
				{//Ver
					FirmwareINFO.FirmwareVersion[k]=FirmwareInfoTempbuff_USB[k+INFOInputVerPtr_t];

				}

				FirmwareINFO.FirmwareSize=0;//计算前清零
				for(uint8_t m=0;m<(INFOInputMD5Ptr_t-5-INFOInputSizePtr_t);m++)
				{//Size
					uint32_t TempCal_t=1;	
					for(uint8_t LoopCnt_t=(INFOInputMD5Ptr_t-5-INFOInputSizePtr_t)-m-1;LoopCnt_t;	LoopCnt_t--)
					{
						TempCal_t=TempCal_t*10;
					}
					FirmwareINFO.FirmwareSize+=(FirmwareInfoTempbuff_USB[INFOInputSizePtr_t+m]-48)*TempCal_t;

				}
				for(uint8_t n=0;n<16;n++)
				{//MD5
					uint8TransToHEXchar(FirmwareInfoTempbuff_USB[INFOInputMD5Ptr_t+n],&FirmwareINFO.Firmware_MD5[2*n],0);

				}	
				break;
			}
		}
	}
	

}
#endif
#endif

#if defined __OTA_H|| defined Bootloader

/**
 * @brief 下载完成后向bootloader传递升级参数的函数（通过向固定的Flash地址写入参数传递升级参数）
 * @param uint8_t UpdateFlagCode_t; 0-未准备进行升级 1-重启后立即升级固件 2-固件下载完成但是不立即升级 3-升级过程失败
 * @retval 
 * @author ZCD1300 
 * @Time 2022年4月14日
*/
uint8_t BootloaderIAP_ParamWrite(uint8_t UpdateFlagCode_t)
{
	uint8_t BootloaderFlagBuff[4]={0};
	if(UpdateFlagCode_t==0)
	{//取消升级
		BootloaderFlagBuff[0]='N';//是否可以升级（Y/N）
		BootloaderFlagBuff[1]='N';//什么时候升级（N/F）now/future
		BootloaderFlagBuff[2]='R';//是否重启（R）
		BootloaderFlagBuff[3]='F';//什么时候重启（N/F）
		Flash_Write(BootloadIAPCtrlFlagFlash_Ptr,1,(uint32_t *)BootloaderFlagBuff);
		return 0;
	}
	else if(UpdateFlagCode_t==1)
	{//重启后升级
		BootloaderFlagBuff[0]='Y';
		BootloaderFlagBuff[1]='N';
		BootloaderFlagBuff[2]='R';
		BootloaderFlagBuff[3]='N';
		Flash_Write(BootloadIAPCtrlFlagFlash_Ptr,1,(uint32_t *)BootloaderFlagBuff);
		return 0;
	}	
	else if(UpdateFlagCode_t==2)
	{
		BootloaderFlagBuff[0]='Y';
		BootloaderFlagBuff[1]='F';
		BootloaderFlagBuff[2]='R';
		BootloaderFlagBuff[3]='N';
		Flash_Write(BootloadIAPCtrlFlagFlash_Ptr,1,(uint32_t *)BootloaderFlagBuff);
		return 0;		
	}
	else
	{
		BootloaderFlagBuff[0]='E';
		BootloaderFlagBuff[1]='R';
		BootloaderFlagBuff[2]='R';
		BootloaderFlagBuff[3]='O';
		Flash_Write(BootloadIAPCtrlFlagFlash_Ptr,1,(uint32_t *)BootloaderFlagBuff);
		return 0;			
	}
}


#endif

