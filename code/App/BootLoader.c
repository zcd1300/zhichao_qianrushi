/*
 * @brief  Boot loader
 * @time 2021 12 22
 * @paramer zcd 
*/
#include "BootLoader.h"
#include "cmsis_armcc.h"
#include "LCD.h"
#include "IAP.h"

uint8_t v1[]="********************************\r\n";
uint8_t v2[]="*          BootLoader          *\r\n";
uint8_t v3[]="*      V0.2.3  Beta    ZCD     *\r\n";
uint8_t v4[]="********************************\r\n";

/**
 * @brief 跳转APP函数实体
 * @param	uint32_t APP_addr App地址
 * @retval uint8_t 
 * @author ZCD1300
 * @Time 2021年12月22日
*/
	uint32_t APP_Addr_t=0;	
typedef void (*jumpAPP)(void);
jumpAPP JumpAppStart;
uint8_t LoadAPP(uint32_t APP_addr)
{

	APP_Addr_t=APP_addr+4;
	if(((*(__IO uint32_t*)APP_addr) & 0x2FFE0000 ) == 0x20000000)//检查APP栈指针是否合法
	{
		__disable_irq();		
		JumpAppStart=(jumpAPP)*(__IO uint32_t*) APP_Addr_t;//指向函数段开始地址	
		__set_MSP(*(__IO uint32_t*) APP_Addr_t);//设置栈顶指针
		JumpAppStart();//开始跳转
	}
	else
	{
		return 1;
	}
}
/**
 * @brief APP跳转控制函数，实际应调用此函数
 * @param	void 
 * @retval int8_t return  0没有AP连接 1有AP连接
 * @author ZCD1300
 * @Time 2021年12月22日
*/
uint8_t Jump_To_APPDisk(uint8_t APPDisk_NUM)
{
	if(APPDisk_NUM==APPDisk_1)
	{
		LoadAPP(FLASH_APP1_ADDR);

	}
	else if(APPDisk_NUM==APPDisk_2)
	{
		//LoadAPP(FLASH_APP2_ADDR);
	}
	else
	{//error
	
	}
}
/**
 * @brief 检查升级标志
 * @param	void 
 * @retval uint8_t
 * @author ZCD1300
 * @Time 2022年4月14日
*/
uint8_t CheckIAPFlag(void)
{
	uint8_t IAPReadBuff_t[4]={0};
	Flash_Read(BootloadIAPCtrlFlagFlash_Ptr,1,(uint32_t *)IAPReadBuff_t);

	/*	Y N R N 对应四个字节； 
			1：Y/N表示是否升级 2：N/F什么时候升级（now/feature） 
			3：R 是否重启		  4：N/F 什么时候重启
	*/
	if(IAPReadBuff_t[0]=='Y')
	{//需要升级
		if(IAPReadBuff_t[1]=='N')
		{//立刻升级
			DebugPrint_uart_LineFeed("Upgrading firmware. Do not disconnect the power supply.",200);
			FirmwareFlashCopy();
			return 0;
		}
		else if(IAPReadBuff_t[1]=='F')
		{//稍后升级
			DebugPrint_uart_LineFeed("The upgradeable firmware has been downloaded, please upgrade as soon as possible.",200);
			return 1;
		}
	}
	else if(IAPReadBuff_t[0]=='N')
	{//不需要升级
		DebugPrint_uart_LineFeed("Firmware version checked.",200);
		return 1;
	}
	else if(IAPReadBuff_t[0]=='E')
	{//升级失败
		DebugPrint_uart_LineFeed("Upgrade failed. Retry manually or contact maintenance personnel.",200);
		return 3;	
	}
	else
	{//升级标志数据无效
		DebugPrint_uart_LineFeed("Error in upgrade flag, please contact personnel for inspection.",200);
		return 2;
	}
}

FirmwareINFO_t FirmwareINFO_Bootloader={0};//读取固件信息存储结构体
uint8_t BootloaderCpyFirmwareFlag=0;
/**
 * @brief Firmware Copy 固件拷贝
 * @param	void 
 * @retval uint8_t 
 * @author ZCD1300
 * @Time 2022年4月14日
*/
uint8_t FirmwareFlashCopy(void)
{
	{//读取固件信息
		Update_FirmwareINFO();
		uint8_t FirmwareINFO_RAW[FirmwareINFO_Offset]={0};
		Flash_Read(FirmwareDownloadAreaFlash_StartPtr,FirmwareINFO_Size,(uint32_t *)FirmwareINFO_RAW);
		if (FirmwareINFO_RAW[8]=='Y')
		{
			/* 存在有效固件 */
			memcpy(FirmwareINFO_Bootloader.FirmwareType,FirmwareINFO_RAW,4*FirmwareINFO_Size);
		}
		else
		{
			return 1;//固件信息无效
		}
	}
	{//固件转存前准备
		EraseAPP1AreaFlash();//擦除现有固件数据
		PackageTotal=FirmwareINFO_Bootloader.FirmwareSize/1024;//计算一个固件需要需要分多少包
		if (PackageTotal*1024< FirmwareINFO_Bootloader.FirmwareSize)
		{
			PackageTotal++;
			LastPackNotFull=1;//最后一个包大小不完整
		}
		else
		{
			LastPackNotFull=0;
		}
		BootloaderCpyFirmwareFlag=1;
	}
	{//读取下载区的新固件并转存到执行区
		uint32_t LessThan1024Leth_bootloader=0;
		uint32_t FirmwareADDRPtr=FirmwareDownloadAreaFlash_StartPtr+FirmwareINFO_Offset;
		FlashOperatePtr=FLASH_APP1_ADDR;//重定向固件搬运的目的地址
		for(uint16_t i=0;i<PackageTotal;i++)
		{		
			if(i==(PackageTotal-1))
			{
				LessThan1024Leth_bootloader=FirmwareINFO.FirmwareSize%1024;
				if((LessThan1024Leth_bootloader%4)!=0)
				{
					LessThan1024Leth_bootloader=(LessThan1024Leth_bootloader/4)+1;
				}
				else
				{LessThan1024Leth_bootloader=LessThan1024Leth_bootloader/4;}
				Read_FirmwarePackage_To_RAMBuff(FirmwareADDRPtr,LessThan1024Leth_bootloader,(uint32_t*)IAP_packageBuff);
				FirmwareADDRPtr+=(FirmwareINFO.FirmwareSize%1024);		
				Write_FirmwarePackageBuff_To_Flash(IAP_packageBuff,LessThan1024Leth_bootloader*4);
			}
			else
			{
				Read_FirmwarePackage_To_RAMBuff(FirmwareADDRPtr,1024/4,(uint32_t*)IAP_packageBuff);
				FirmwareADDRPtr+=1024;
				Write_FirmwarePackageBuff_To_Flash(IAP_packageBuff,1024);
			}
			//UART_Print_Percentage_progress(PackageTotal,i);//这是用来显示固件拷贝进度的函数，但是如果要显示进度并串口打印，会增加Flash 的操作次数，严重拖慢拷贝速度
		}
		PackageTotal=0;
	}
	{//MD5校验
		if(FirmwareMD5HashCheck(FLASH_APP1_ADDR)==0)
		{//MD5相同
			BootloaderIAP_ParamWrite(0);
			DebugPrint_uart_LineFeed("Firmware Upgrade Complete.",200);
			return 0;
		}
		else
		{
			BootloaderIAP_ParamWrite(3);
			DebugPrint_uart_LineFeed("Firmware upgrade error(MD5), please retry Manually the upgrade.",200);
			return 2;
		}//md5不同
	}

}



