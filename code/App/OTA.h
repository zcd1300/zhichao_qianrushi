#ifndef __OTA_H
#define __OTA_H

#include "hw_abst.h"
#include "md5.h"
#include "ESP8266.h"
#include "tim.h"
#include "host_app.h"


int8_t OTA_Ctrl(void);
int8_t OTA_FirmwareDownload(uint8_t * packageBuff_t,uint16_t SinglePackSize_t,FirmwareINFO_t TargetFirmwareINFO_t);
int8_t OTA_FirmwareAutoCheck(void);

#endif 

