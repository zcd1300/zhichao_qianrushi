#include "hw_abst.h"
#include "gpio.h"
#include "lcd.h"

#if defined Bootloader
	
#endif
#if defined HostAPP
	#include "http_json.h"
	#include "host_app.h"
	#include "tim.h"
	
#endif

 /**
 * @brief  System reset //启动系统复位请求以重置MCU
 * @param	void
 * @retval void
 * @author ZCD1300
 * @Time 2021年11月22日
*/
void MCU_System_RST(void)
{
	HAL_NVIC_SystemReset();
}
 /**
 * @brief  串口硬件抽象层接口，适配不同芯片只需要修改此接口 
 * @param	void
 * @retval void
 * @author ZCD1300
 * @Time 2021年10月19日
*/
void UART_Send(UART_HandleTypeDef *huart, uint8_t *pData, uint16_t Size, uint32_t Timeout)
{
	HAL_UART_Transmit(huart,pData,Size,Timeout);
}

 /**
 * @brief  串口2硬件抽象层接口，适配不同芯片只需要修改此接口,专用与Debug 固定串口2		pa2-TX pa3-RX
 * @param	void
 * @retval void
 * @author ZCD1300
 * @Time 2021年11月10日
*/
void DebugPrint_uart(char *pData,  uint32_t Timeout)
{
	uint16_t Size=strlen(pData);
	HAL_UART_Transmit(&huart2,pData,Size,Timeout);
	usb_hid_Tx(pData);
	LCD_StringBUFF_Manage(pData);
//	Debug_Uart_Display(pData,1);
}
void DebugPrint_uart_LineFeed(char *pData,  uint32_t Timeout)
{
	//malloc()
	uint16_t Size_t=strlen(pData);
	char Send_temp[500]={0};
	
	strcpy(Send_temp,"\r\n");
	strcpy(Send_temp+2,pData);
	strcpy(Send_temp+2+Size_t,"\r\n");
	
	//DebugPrint_uart("\r\n",Timeout);
	DebugPrint_uart(Send_temp,Timeout);	
	//DebugPrint_uart("\r\n",Timeout);
}
 /**
 * @brief  GPIO硬件抽象层接口，适配不同芯片只需要修改此接口 
 * @param	void
 * @retval void
 * @author ZCD1300
 * @Time 2021年12月9日
*/
GPIO_PinState GPIO_Read(GPIO_TypeDef* GPIOx_t, uint16_t GPIO_Pin_t)
{
	GPIO_PinState GPIO_bitstatus;
	GPIO_bitstatus=HAL_GPIO_ReadPin(GPIOx_t,GPIO_Pin_t);
	return GPIO_bitstatus;
}
 /**
 * @brief  GPIO KEY //三枚KEY的电平读取，放在循环中一直检测
 * @param	void
 * @retval void
 * @author ZCD1300
 * @Time 2021年12月9日
*/
uint8_t KeyLastState[3]={0};
uint8_t KeyState[3]={0};
void Key_read(void)
{
	
	if(GPIO_Read(GPIOF,GPIO_PIN_0)==GPIO_PIN_RESET)
	{
		Delay(10);
		if(GPIO_Read(GPIOF,GPIO_PIN_0)==GPIO_PIN_RESET)
		{
			KeyState[0]=0;
		}
		else
		{	KeyState[0]=1;}
	}
	else
	{KeyState[0]=1;}
	
	if(GPIO_Read(GPIOF,GPIO_PIN_1)==GPIO_PIN_RESET)
	{
		Delay(10);
		if(GPIO_Read(GPIOF,GPIO_PIN_1)==GPIO_PIN_RESET)
		{
			KeyState[1]=0;
		}	
		else
		{	KeyState[1]=1;}		
	}
	else
	{KeyState[1]=1;}	
	
	if(GPIO_Read(GPIOF,GPIO_PIN_2)==GPIO_PIN_RESET)
	{
		Delay(10);
		if(GPIO_Read(GPIOF,GPIO_PIN_2)==GPIO_PIN_RESET)
		{
			KeyState[2]=0;
		}
		else
		{	KeyState[2]=1;}		
	}	
	else
	{KeyState[2]=1;}	
	
	KeyLastState[0]=KeyState[0];
	KeyLastState[1]=KeyState[1];
	KeyLastState[2]=KeyState[2];
}
 /**
 * @brief  LED Control //三枚LED的控制
 * @param	void
 * @retval void
 * @author ZCD1300
 * @Time 2021年12月21日
*/
void LED_ctrl(uint8_t LEDID,uint8_t LED_state)
{
	switch(LEDID)
	{
		case 0:
		{
			if(LED_state!=0)
			{
				HAL_GPIO_WritePin(GPIOG, GPIO_PIN_10,GPIO_PIN_RESET);
			}
			else
			{
				HAL_GPIO_WritePin(GPIOG, GPIO_PIN_10,GPIO_PIN_SET);
			}
			break;
		}
		case 1:
		{
			if(LED_state!=0)
			{
				HAL_GPIO_WritePin(GPIOG, GPIO_PIN_11,GPIO_PIN_RESET);
			}
			else
			{
				HAL_GPIO_WritePin(GPIOG, GPIO_PIN_11,GPIO_PIN_SET);
			}			
			
			break;
		}
		case 2:
		{
			if(LED_state!=0)
			{
				HAL_GPIO_WritePin(GPIOG, GPIO_PIN_12,GPIO_PIN_RESET);
			}
			else
			{
				HAL_GPIO_WritePin(GPIOG, GPIO_PIN_12,GPIO_PIN_SET);
			}			
			
			break;
		}	
		default:
		{break;}
	}

}
 /**
 * @brief  Zigbee module Rst pin ctrl //zigbee 复位引脚控制
 * @param	uint8_t Pin_State_t
 * @retval void
 * @author ZCD1300
 * @Time 2022年4月16日
*/
void ZigbeeModule_RstPINCtrl(uint8_t Pin_State_t)
{
	if(Pin_State_t==0){
	HAL_GPIO_WritePin(GPIOD,GPIO_PIN_12,GPIO_PIN_RESET);
	}
	else{
	HAL_GPIO_WritePin(GPIOD,GPIO_PIN_12,GPIO_PIN_SET);
	}

}

#if defined Bootloader
uint16_t StringSecanPointer_Assign_Buff=0;
 /**
 * @brief  检索特定字符串（最多检索20个特征字符）（可指定检索的区域） 
 * @param	 检索特征字符，指定的检索池,字符匹配长度（<=20），检索深度
 * @retval	检索结果 1-是  0-否
 * @author ZCD
 * @Time 2021年11月13日
*/
uint8_t StringSearch_Assign(char *cha,uint8_t *Assign_Buff,uint8_t Len/*Len<=10*/,uint16_t Deep)
{
	uint16_t CntMax=Deep;
	uint8_t CheckBuff[20]={0};
	
	for(uint16_t i=0;i<CntMax;i++)
	{
			uint8_t flag=1;
		CheckBuff[0]=Assign_Buff[i];
		CheckBuff[1]=Assign_Buff[i+1];
		CheckBuff[2]=Assign_Buff[i+2];
		CheckBuff[3]=Assign_Buff[i+3];
		CheckBuff[4]=Assign_Buff[i+4];
		CheckBuff[5]=Assign_Buff[i+5];
		CheckBuff[6]=Assign_Buff[i+6];
		CheckBuff[7]=Assign_Buff[i+7];
		CheckBuff[8]=Assign_Buff[i+8];
		CheckBuff[9]=Assign_Buff[i+9];
		
		CheckBuff[10]=Assign_Buff[i+10];
		CheckBuff[11]=Assign_Buff[i+11];
		CheckBuff[12]=Assign_Buff[i+12];
		CheckBuff[13]=Assign_Buff[i+13];
		CheckBuff[14]=Assign_Buff[i+14];
		CheckBuff[15]=Assign_Buff[i+15];
		CheckBuff[16]=Assign_Buff[i+16];
		CheckBuff[17]=Assign_Buff[i+17];
		CheckBuff[18]=Assign_Buff[i+18];
		CheckBuff[19]=Assign_Buff[i+19];			
		for(uint8_t j=0;j<Len;j++)
		{
			if(CheckBuff[j]!=cha[j])
			{
				flag=0;
			}
		}
		if(flag==1)
		{
			StringSecanPointer_Assign_Buff=i;
			return 1;
		}
	}

	return 0;
}

#endif 
#if defined HostAPP
/**
 * @brief 定时器中断回调函数	
 * @param	TIM_HandleTypeDef *htim	//定时器结构体
 * @retval void	
 * @author ZCD1300
 * @Time 2021年11月15日
*/
uint16_t tim3_cnt=0;
uint16_t tim3CNT_Zigbee=0;
uint16_t tim3CNT_TCP=0;
uint16_t tim3AutoCheckFirmware_cnt=0;
void HAL_TIM_PeriodElapsedCallback(TIM_HandleTypeDef *htim)
{
	if(htim==&htim3)
	{//定时器3中断，10s进入一次中断
		//注意，开启定时器中断时会进入一次

		Local_time_refresh(10);//本地时间刷新、中断周期10s
		if(Debug_enable_flag!=1)
		{
			tim3CNT_TCP++;
			if(tim3CNT_TCP>1)
			{
				tim3CNT_TCP=0;

				//Check_TCP_Connection();//这个函数应该在Debug功能开启的时候临时跳过					
			}		
		}
		{//屏幕自动息屏
			//由屏幕内部实现
//			DisplaySleep_TimeCNT++;
//			if(DisplaySleep_TimeCNT>=2)
//			{
//				//DisplaySleep_TimeCNT=0;
//				LCD_Status(Sleep_Status);
//				Delay(10);
//				LCD_Status(Sleep_Status);
//			}				
		}
		tim3_cnt++;
		tim3CNT_Zigbee++;
		if((tim3_cnt>6))
		{//1分钟检测一次 token有效性
			tim3_cnt=0;			
			if(!Host_utoken_Check_SmartNest())
			{//token有效或者刷新成功
			}
			else
			{//token重新获取超时(10s)，或者连接到服务器失败

				//Reg_User_Check();//返回用户登录界面，这里登录失败不再自动返回登录页面，会一直重试，需要手动切换到登录页面
			}
		}

		if((tim3CNT_Zigbee>360)&&(UploadingFlag==0))
		{//十分钟检测一次 在线的Zigbee从机数量
			tim3CNT_Zigbee=0;
			if(Debug_enable_flag!=1)//debug模式不自动检查
			{
				zigbee_Refesh_OnlineDeviceNUM();//定时检测zigbee网络终端在线状态
				page1();
			}
		}

		{//自动检查固件升级
			tim3AutoCheckFirmware_cnt++;
			if((tim3AutoCheckFirmware_cnt>(6*30))&&(UploadingFlag==0))
			{//（凌晨0~3点）30分钟检查一次
				tim3AutoCheckFirmware_cnt=0;			
				OTA_FirmwareAutoCheck();
			}
		}
	}

}

/**
 * @brief 定时器中断控制函数  ()
 * @param TIM_HandleTypeDef *htim (&htim3),uint8_t IT_code(0/1 0-关中断，1开中断)
 * @retval void
 * @Time 2022年4月13日
*/
void TIM_IT_Ctrl(TIM_HandleTypeDef *htim,uint8_t IT_code)
{
	if(IT_code!=0)
	{
		HAL_TIM_Base_Start_IT(htim);
	}
	else
	{
		HAL_TIM_Base_Stop_IT(htim);
	}
	
}
#endif
/**
 * @brief  Clear_Buff 	//清空buff
 * @param	void
 * @retval void
 * @author ZCD1300
 * @Time 2021年11月10日
*/
void Clear_Buff(char *Buff,uint16_t Len,uint16_t Buff_Max)
{
	for(uint16_t i=0;i<=Val_MAX_Limit_U(Len+2,Buff_Max);i++)
	{
		Buff[i]=0;
	}
	
}
/**
 * @brief  字符串匹配  （这个函数本来不应该算硬件抽象层，但是常用，于是放到抽象层）
 * @param	 char* input ,uint8_t input_Len,char* target,uint8_t target_Len  (如果懒得写长度就都填0，让函数自己计算)
 * @retval uint8_t 1 表示匹配成功 0不成功	（注意这里是 成功为1，方便if语句判断）
 * @author ZCD1300
 * @Time 2021年11月7日
*/
uint8_t StringMatch(char* input ,uint8_t input_Len,char *target,uint8_t target_Len)
{
	uint8_t temp_Cnt=0;
	if((input_Len==0)&&(target_Len==0))
	{
		input_Len= strlen(input);
		target_Len=strlen(target);
	
	}
	if(input_Len > target_Len)
	{
		return 0;
	}
		
	for(uint8_t i=0;i<input_Len;i++)
	{
		if(input[i]==target[i])
		{
			temp_Cnt++;
		}
	}
	if(temp_Cnt==input_Len)
	{
		return 1;
	}
	else
	{return 0;}
	
	return 0;
}

 /**
 * @brief  最大输入数值限制(无符号)  （这个函数本来不应该算硬件抽象层，但是常用，于是放到抽象层）
 * @param	 uint16_t inputNUM uint16_t MAX_NUM  
 * @retval uint16_t 直接返回范围内的数值
 * @Time 2021年11月8日
*/
uint16_t Val_MAX_Limit_U(uint16_t inputNUM,uint16_t MAX_NUM)
{
	if(inputNUM<=MAX_NUM)
	{
		return inputNUM;
	}
	else{return MAX_NUM;}
}
 /**
 * @brief  延时函数  
 * @param	 uint32_t delay_Time
 * @retval void
 * @Time 2021年11月11日
*/
void Delay(uint32_t delay_Time)
{
	HAL_Delay(delay_Time);
}

 /**
 * @brief  Wait_Until_FlagTrue  //等待直到Flag为Ture;注意进入函数 不会 将Flag自动清零，要收到置位
 * @param	 uint8_t *Flag uint32_t Timeout 单位ms,最大可在头文件修改,如果输入0，则不限制超时时间；
 * @retval int8_t return //0超时  1触发成功
 * @Time 2021年11月14日
*/
uint8_t Inv_optim_t=0;

int8_t Wait_Until_FlagTrue(uint8_t *Flag,uint32_t Timeout)
{
  uint32_t tickstart = HAL_GetTick();
  uint32_t wait = Timeout;
	Inv_optim_t=*Flag;
	if(wait==0)
	{//不设置超时时间
		while(*Flag==0)
		{
			Inv_optim_t=*Flag;			
		}
		return 1;//触发成功
	}
	else
	{
		if (wait < WaitUntilFlagTrue_MAX)
		{
			wait += (uint32_t)(uwTickFreq);
		}
		
		while((HAL_GetTick() - tickstart) < wait)//超时时间之内
		{
			if(*Flag==1)
			{//未超时，触发成功
				return 1;
			}

		}
		//等待超时
		return 0;		
	}
}
/**
 * @brief Uart_Debug_CMD_ADD	//串口Debug新增指令
 * @param void	如果懒得输入CmdLen_t 就填0，让函数自己计数，但是可能存在长度不匹配。
 * @retval void	1表示匹配成功，0表示不成功
 * @author ZCD1300
 * @Time 2021年11月15日
*/
int8_t Uart_Debug_CMD_ADD(char *Cmd_t,uint8_t *UartBuff,uint8_t CmdLen_t,uint16_t MatchDeep_t)
{
	if(CmdLen_t==0)
	{
		CmdLen_t=strlen(Cmd_t);
	}
	if(StringSearch_Assign(Cmd_t,UartBuff,CmdLen_t,MatchDeep_t))
	{
		Clear_Buff(UartBuff,MatchDeep_t,BuffSIZE_MAX2);
		return 1;
	}		
	else
	{return 0;}	
}
/**
 * @brief uint8_t to double HEX char 8位整数转两位HEX字符 表示
 * @param uint8_t InputValue-输入数值；char* OutputCharsPtr-输出到的目的地址; uint8_t CapitalSet-输出大小写控制
 * @retval uint8_t
 * @author ZCD1300
 * @Time 2022年4月7日
*/
uint8_t uint8TransToHEXchar(uint8_t InputValue,char* OutputCharsPtr,uint8_t CapitalSet)
{
	uint8_t High4Bits=0;
	uint8_t Low4Bits=0;
	High4Bits=InputValue>>4;
	Low4Bits=InputValue & 0x0f;
	
	if(High4Bits<10)
	{
		OutputCharsPtr[0]=High4Bits+48;
	}
	else if(CapitalSet)
	{//大写输出
		OutputCharsPtr[0]=High4Bits-10+65;
	}
	else
	{//小写输出
		OutputCharsPtr[0]=High4Bits-10+97;
	}

	if(Low4Bits<10)
	{
		OutputCharsPtr[1]=Low4Bits+48;
	}
	else if(CapitalSet)
	{//大写输出
		OutputCharsPtr[1]=Low4Bits-10+65;
	}
	else
	{//小写输出
		OutputCharsPtr[1]=Low4Bits-10+97;
	}
	return 0;
}

/**
 * @brief 串口打印加载进度（百分比）
 * @param	int
 * @retval uint8_t return 
 * @author ZCD1300 
 * @Time 2022年4月14日
*/
void UART_Print_Percentage_progress(uint32_t OverallProgress_t,uint32_t CurrentProgress_t)
{
	float OverallProgress=OverallProgress_t;
	float CurrentProgress=CurrentProgress_t;
	char PrintTemplate_t[10]="** 100.0%";
	char outputTemp[16]={0};
	float Cal_Temp1=0.0;
	if(CurrentProgress==OverallProgress)
	{//100%
		DebugPrint_uart_LineFeed(PrintTemplate_t,200);
	}
	else
	{
		Cal_Temp1=(CurrentProgress*100)/OverallProgress;
		sprintf(outputTemp,"** %.2f%%",Cal_Temp1);		
		DebugPrint_uart_LineFeed(outputTemp,200);
		
	}
	return ;
}

