 /**
 * @brief  cc2530相关的控制（非官方固件）
 * @param	void
 * @retval	void
 * @author ZCD
 * @Time 2021年11月26日
*/
//cc2530 这里用预留的UART4
#include "cc2530.h"
#include "ESP8266.h"//只依赖其中的StringSearch_Assign()函数，用来检索字符串；

//#include "http_json.h"
//#include "host_app.h"


uint8_t DeviceOnline_Falg[DeviceNUM_MAX]={0};
uint8_t OnlineDeviceCount=0;
 /**
 * @brief  ZigBee Device num refesh//刷新在线的从机数量
 * @param	void
 * @retval	void
 * @author ZCD
 * @Time 2021年12月3日
*/
uint8_t zigbee_Refesh_OnlineDeviceNUM(void)
{
	OnlineDeviceCount=0;
	ReadAllSensorFlag=0;
	for(uint8_t i=1;i<=DeviceNUM_MAX;i++)
	{
		char Refesh_OnlineDeviceNUMBuff[50]={0};
		sprintf(Refesh_OnlineDeviceNUMBuff,"Reading Device %u MAC...",i);
		DebugPrint_uart_LineFeed(Refesh_OnlineDeviceNUMBuff,200);
		if (zigbee_Read_MAC(i,DeviceSNList)!=0)
		{//读取失败，认为终端离线
			DebugPrint_uart("Timeout MAC.",200);
		}
		else
		{//设备在线
			DebugPrint_uart("MAC OK.",200);
			OnlineDeviceCount++;
			Device_BUFF_T[0]=0;	
		}
	}
	return 0;
}
 /**
 * @brief  ZigBee net initial//初始化Zigbee 网络
 * @param	void
 * @retval	void
 * @author ZCD
 * @Time 2021年12月3日
*/
uint8_t ZGRST_OK_Flag=0;
uint8_t ZigBeeNET_Init(void)
{
	uint8_t ZGRST_wait_cnt=0;
	DebugPrint_uart("** ...Z\r\n",0x200);//完成	
	if(GPIO_Read(GPIOF,GPIO_PIN_2)==RESET)
	{//key3 按下，直接跳过ZigBeeNET_Init
		ZGRST_OK_Flag=1;
		//zigbee_Refesh_OnlineDeviceNUM();
		DebugPrint_uart("Skip ZigBee module initialization!\r\n",0x200);//完成	
		DebugPrint_uart("||----ZigBee net initialization completed----||\r\n\r\n",0x200);//完成	
		HAL_GPIO_WritePin(GPIOD,GPIO_PIN_12,GPIO_PIN_SET);	
		return 1;//跳过RST检查
	}
	
	ZGRST_Retry:
	{//协调器复位
		HAL_GPIO_WritePin(GPIOD,GPIO_PIN_12,GPIO_PIN_RESET);	
		Delay(100);
		HAL_GPIO_WritePin(GPIOD,GPIO_PIN_12,GPIO_PIN_SET);		
	}
	//等待复位完成
	while((!StringSearch_Assign("say Hello",Rx_Buff3,9,100))&&0)
	{
		{//跳过检查ZigBeeNET_Init
			if(GPIO_Read(GPIOF,GPIO_PIN_2)==RESET)
			{//key3 按下，直接跳过ZigBeeNET_Init
				ZGRST_OK_Flag=1;
				zigbee_Refesh_OnlineDeviceNUM();
				DebugPrint_uart("Skip ZigBee module initialization!\r\n",0x200);//完成	
				DebugPrint_uart("||----ZigBee net initialization completed----||\r\n\r\n",0x200);//完成	
				ZGRST_wait_cnt=0;
				return 1;//跳过RST检查
			}			
		}
		ZGRST_wait_cnt++;
		Delay(2000);
		if((ZGRST_wait_cnt%2)==1)
		{
			DebugPrint_uart("** Wait for ZigBee module RST.\r\n",0x200);		
		}
		if(ZGRST_wait_cnt>=10)
		{
			DebugPrint_uart("** Retry reset ZigBee module.\r\n",0x200);	
			ZGRST_wait_cnt=0;
			goto ZGRST_Retry;
		}
	}	
	ZGRST_OK_Flag=1;
	DebugPrint_uart("** ZigBee net RST OK.\r\n",0x200);
	DebugPrint_uart("** ZigBee net checking...\r\n",0x200);
	{//收集Zigbee网络信息
	  //device 有效数量，标记ID是否在线，同时读取MAC对应到ID
	  zigbee_Refesh_OnlineDeviceNUM();
			char OnlinezgbeePrintTemp[40]={0};
			sprintf(OnlinezgbeePrintTemp,"** 1 2 3 4 5 6\r\n   %d %d %d %d %d %d"\
							,DeviceOnline_Falg[0],DeviceOnline_Falg[1],DeviceOnline_Falg[2]\
							,DeviceOnline_Falg[3],DeviceOnline_Falg[4],DeviceOnline_Falg[5]);
			DebugPrint_uart_LineFeed(OnlinezgbeePrintTemp,400);			

	}
	DebugPrint_uart("||----ZigBee net initialization completed----||\r\n\r\n",0x200);//完成	
	return 0;
}

uint8_t Recv_BUFF_cc2530[RecvBUFF_cc2530_MAX]={0};
uint8_t SendCMDDisplayFlag=0;
uint8_t ReceiveDisplayFlag=0;
 /**
 * @brief  串口发送,解耦硬件层和软件驱动
 * @param	char Send_temp,uint16_t Send_len_t;发送内容和数据长度
 * @retval	void
 * @author ZCD
 * @Time 2021年11月26日
*/
int8_t Uart_Send_cc2530(char* Send_temp,uint16_t Send_len_t)
{
	
	UART_Send(&huart3,Send_temp,Send_len_t,0x400);
	char ZgCMDsendBuff[30]={0};
	if(SendCMDDisplayFlag==1)
	{
		sprintf(ZgCMDsendBuff,"Tx: ");
		for(uint8_t i=0;i<6;i++)
		{
			sprintf(ZgCMDsendBuff+4+(2*i),"%2X",Send_temp[i]);
		}
		DebugPrint_uart_LineFeed(ZgCMDsendBuff,200);
	}

	return 0;
}
 /**
 * @brief  串口数据转存
 * @param	viod
 * @retval	void
 * @author ZCD
 * @Time 2021年11月26日
*/
void UartBUFF_TranSave_cc2530(void)
{
	char head_t[2]={CMD_Head1_cc2530,CMD_Head2_cc2530};
	StringSearch_Assign(head_t,Rx_Buff3,2,100);//寻找数据头
	memmove(Recv_BUFF_cc2530,Rx_Buff3+StringSecanPointer_Assign_Buff,RecvBUFF_cc2530_MAX);
	// for(uint8_t j=0;j<RecvBUFF_cc2530_MAX;j++)
	// {
	// 	Recv_BUFF_cc2530[j]=Rx_Buff3[j+StringSecanPointer_Assign_Buff];
	// }	
	Rx_Buff3[StringSecanPointer_Assign_Buff]=0;//销毁头标志
	Rx_Buff3[StringSecanPointer_Assign_Buff+1]=0;
}
/**
 * @brief  头尾校验
 * @param	viod
 * @retval	uint8_t return //0表示不正确，1表示校验成功
 * @author ZCD
 * @Time 2021年11月26日
*/
uint16_t RetData_Len=0;
uint8_t zigbee_Head_End_Check(void)
{
	uint8_t Check_cnt_t =0;
	if(Recv_BUFF_cc2530[0] == CMD_Head1_cc2530)
	{Check_cnt_t++;}
	if(Recv_BUFF_cc2530[1] == CMD_Head2_cc2530)
	{Check_cnt_t++;}	
	uint16_t End_point_t =0;
	while(1)
	{
		if((Recv_BUFF_cc2530[End_point_t]==CMD_End1_cc2530)&&(Recv_BUFF_cc2530[End_point_t+1]==CMD_End2_cc2530))
		{
			RetData_Len=End_point_t;
			//End_point_t =0;
			Check_cnt_t+=2;
			break;
		}
		End_point_t++;
		if((End_point_t+1)>=RecvBUFF_cc2530_MAX)
		{
			break;//接收缓冲区空间不足
		}
	}
	if(Check_cnt_t==4)
	{//数据头尾正常
		return 1;
	}
	else
	{return 0;}
}

uint8_t Device_BUFF_T[RecvBUFF_cc2530_MAX]={0}; 
uint8_t temp_delayFlag=0;
 /**
 * @brief  读取数据，并转存到Sensor_BUFF_T
 * @param	char Send_temp,uint16_t Send_len_t;发送内容和数据长度
 * @retval	0-数据正常；1-数据超时；2-数据格式错误
 * @author ZCD
 * @Time 2021年11月26日
*/
int8_t zigbee_ReadData(uint8_t Device_num,uint8_t Sensor_type)
{
	char Send_Buff_t[6]={CMD_Head1_cc2530,CMD_Head2_cc2530,Device_num,Sensor_type,CMD_End1_cc2530,CMD_End2_cc2530};
	Uart_Send_cc2530(Send_Buff_t,6);
	if(temp_delayFlag==1)
	{
		temp_delayFlag=0;
		Delay(10);
	}
	Delay(1);
	UART3_Refresh_Flag=0;//等待串口返回数据
	if(Wait_Until_FlagTrue(&UART3_Refresh_Flag,5000))
	{//5s内收到返回数据
		//数据转存
		Delay(10);
		UartBUFF_TranSave_cc2530();//从buff中转存到Recv_BUFF_cc2530
		Delay(5);
		if(zigbee_Head_End_Check())
		{//数据格式校验通过
			for(uint8_t i=0;i< Val_MAX_Limit_U(RetData_Len,RecvBUFF_cc2530_MAX);i++)
			{
				Device_BUFF_T[i]=Recv_BUFF_cc2530[i+2];//实际有效数据转存（去掉头数据）
				Recv_BUFF_cc2530[i+2]=0;//数据清空
				Recv_BUFF_cc2530[0]=0;
				Recv_BUFF_cc2530[1]=0;
			}

			return 0;//现在那个cc2530还没有收到后的返回值，这里只能直接返回	
		}	
		else
		{return 2;}//数据格式错误
	}
	else
	{
		return 1;//数据接收超时
	}
}
 /**
 * @brief  读取传感器数据
 * @param	char Send_temp,uint16_t Send_len_t;发送内容和数据长度
 * @retval	void
 * @author ZCD
 * @Time 2021年11月26日
*/
float Temperture_D=0.0;
uint32_t RH_D=0;
uint16_t CO2_D=0;
uint16_t NH3_D=0;
uint16_t Lux_D=0;
uint8_t zigbeeReadDataReturn=0;
uint32_t  rh_t=0;
uint32_t temper_t=0;
uint8_t zigbee_Read_Sensor(uint8_t Device_num,uint8_t Sensor_type,Device_t *DeviceSave_t)
{
	if(DeviceOnline_Falg[Device_num-1]!=1)
	{//ID对应的Device未注册
		return 2;
	}
	else
	{
		if(Sensor_type==0x05)//温度 3字节
		{
			uint8_t retTmpCnt=0;
			Retry_temperture:
			temp_delayFlag=1;
			zigbeeReadDataReturn=zigbee_ReadData(Device_num,Sensor_type-1);
			if(zigbeeReadDataReturn==1)
			{//设备离线
				temper_t=0;
				Temperture_D=0.0;
				return 1;
			}
			else if(zigbeeReadDataReturn==2)
			{//数据格式错误，可能是编译器问题，直接重试
				goto Retry_temperture;
			}
			else
			{
				if((Device_BUFF_T[1]!=0xf4)&&(Device_BUFF_T[1]==0x05))
				{
					temper_t=0;
					temper_t=(temper_t|Device_BUFF_T[3])<<8;
					temper_t=(temper_t|Device_BUFF_T[4])<<8;
					temper_t=(temper_t|Device_BUFF_T[5]);					
					//temper_t=(uint32_t)Device_BUFF_T[3]<<16|(uint32_t)Device_BUFF_T[4]<<8|(uint32_t)Device_BUFF_T[5];
					temper_t=temper_t&0x0fffff;	
													
				}
				else if((Device_BUFF_T[1]!=0xf4)&&(Device_BUFF_T[1]==0x04))
				{
					goto Retry_temperture;
				}
				else
				{ temper_t=0;}
			}
			
			Temperture_D=(((double)temper_t/1048576.0)*200)-50;
			if(Temperture_D==0.0)
			{
				if(retTmpCnt<=200)
				{
					retTmpCnt++;
					Delay(10);
					goto Retry_temperture;					
				} 
			}

		}
		else if(Sensor_type==0x04)//湿度 3字节
		{ uint8_t retRHCnt=0;
			//uint32_t  rh_t=0;
			retry_rh_t:
			zigbee_ReadData(Device_num,Sensor_type);
			if(zigbeeReadDataReturn==1)
			{//设备离线
				RH_D=0;
				rh_t=0;
				return 1;
			}			
			else if(zigbeeReadDataReturn==2)
			{
				goto retry_rh_t;
			}
			else
			{
				if((Device_BUFF_T[1]!=0xf4)&&(Device_BUFF_T[1]==0x04))
				{
					rh_t=0;
					rh_t=(rh_t|Device_BUFF_T[3])<<8;
					rh_t=(rh_t|Device_BUFF_T[4])<<8;
					rh_t=rh_t|Device_BUFF_T[5];
					//rh_t=(uint32_t)Device_BUFF_T[3]<<16|(uint32_t)Device_BUFF_T[4]<<8|(uint32_t)Device_BUFF_T[5];
					rh_t=rh_t>>4;
				}
				else if((Device_BUFF_T[1]!=0xf4)&&(Device_BUFF_T[1]==0x05))
				{
					goto retry_rh_t;
				}
				else
				{rh_t=0;}
			}
			
			RH_D=(((double)rh_t/1048576.0))*100;
			if(RH_D==0)
			{
				if(retRHCnt<=200)
				{
					Delay(10);
					retRHCnt++;
					goto retry_rh_t;
				}
			}	
		}
		else if(Sensor_type==0x03)//光照 2字节
		{	uint8_t retLuxCnt=0;
			uint32_t lux_t=0;
			retry_Lux_D:
			zigbeeReadDataReturn=zigbee_ReadData(Device_num,Sensor_type);
			if(zigbeeReadDataReturn==1)
			{//传感器离线
				Lux_D=0;
				lux_t=0;
				return 1;
			}
			else if(zigbeeReadDataReturn==2)
			{
				goto retry_Lux_D;
			}
			else
			{
				if((Device_BUFF_T[1]!=0xf3)&&(Device_BUFF_T[1]==0x03))
				{				
					lux_t=(uint32_t)Device_BUFF_T[4]<<8|(uint32_t)Device_BUFF_T[5];	
				}		
			}
			Lux_D=((float)lux_t*10)/12;
			if(Lux_D==0)
			{
				if(retLuxCnt<=50)
				{
					retLuxCnt++;
					goto retry_Lux_D;
				}
				
			}
		}
		else if(Sensor_type==0x01)//CO2 2字节
		{	uint8_t retCO2Cnt=0;
			uint32_t co2_t=0;
			retry_co2:
			zigbeeReadDataReturn=zigbee_ReadData(Device_num,Sensor_type);
			if(zigbeeReadDataReturn==1)
			{//传感器离线
				CO2_D=0;
				co2_t=0;
				return 1;
			}
			else if(zigbeeReadDataReturn==2)
			{
				goto retry_co2;
			}
			else
			{
				if((Device_BUFF_T[1]!=0xf1)&&(Device_BUFF_T[1]==0x01))
				{
					co2_t=(uint32_t)Device_BUFF_T[4]<<8|(uint32_t)Device_BUFF_T[5];	
				}				
			}
			CO2_D=co2_t;		
			if(CO2_D==0)
			{
				if(retCO2Cnt<=50)
				{
					goto retry_co2;
					retCO2Cnt++;
				}
			}
		}
		else if(Sensor_type==0x02)//NH3 2字节
		{
			uint8_t retNH3Cnt=0;
			uint32_t nh3_t=0;
			retry_nh3:
			zigbeeReadDataReturn=zigbee_ReadData(Device_num,Sensor_type);
			if(zigbeeReadDataReturn==1)
			{//传感器离线
				NH3_D=0;
				nh3_t=0;
				return 1;
			}
			else if(zigbeeReadDataReturn==2)
			{
				goto retry_nh3;
			}
			else
			{
				if((Device_BUFF_T[1]!=0xf2)&&(Device_BUFF_T[1]==0x02))
				{
					nh3_t=(uint32_t)Device_BUFF_T[4]<<8|(uint32_t)Device_BUFF_T[5];	
				}
						
			}
			NH3_D=nh3_t/100;	
			if((nh3_t==0)||(NH3_D>650))
			{
				if(retNH3Cnt<=10)
				{
					goto retry_nh3;
				}
			}	
		}	
		{//把数据转存到指定的结构体一份
			DeviceSave_t[Device_num-1].Temperture=Temperture_D;
			DeviceSave_t[Device_num-1].RH=RH_D;
			DeviceSave_t[Device_num-1].NH3=NH3_D;
			DeviceSave_t[Device_num-1].Lux=Lux_D;
			DeviceSave_t[Device_num-1].CO2=CO2_D;
			//转存完成后数据清空
			for(uint8_t i=0;i<RecvBUFF_cc2530_MAX;i++)
			{
				Device_BUFF_T[i]=0;
			}
		}	
		
	}

}


Device_t DeviceSNList[6]={0};
/**
 * @brief  读取device MAC 并对应到deviceID 的结构体；同时更新在线设备表
 * @param	uint8_t device_ID,Device_t *DeviceList_t
 * @retval	0-读取成功；1-读取超时；2-设备未连接；3-其他错误；9-传入参数错误
 * @author ZCD
 * @Time 2022年4月17日
*/
uint8_t zigbee_Read_MAC(uint8_t device_ID,Device_t *DeviceList_t)
{
	if(device_ID==0)
	{return 9;}//参数错误，deviceID不能是0（0—协调器ID）
	if( zigbee_ReadData(device_ID,0x00))
	{//读取MAC超时或者格式错误
		DeviceOnline_Falg[device_ID-1]=0;
		return 1;
	}
	//到此已经收到了返回的数据
	for(uint8_t j=0;j<16;j++)//清空现存的MAC
	{
		DeviceList_t[device_ID-1].DeviceSN[j]='0';
	}	

	if((Device_BUFF_T[0]==0x00)&&(Device_BUFF_T[1]==0xfe))
	{//从机设备未连接到协调器网络
		DeviceOnline_Falg[device_ID-1]=0;
		char TEmpPrint[90]={0};
		sprintf(TEmpPrint,"Device ID=%u Not connected to the network.",device_ID);
		DebugPrint_uart_LineFeed(TEmpPrint,200);
		return 2;
	}
	else if(Device_BUFF_T[1]==0xff)
	{
		DeviceOnline_Falg[device_ID-1]=0;
		char TEmpPrint[90]={0};
		sprintf(TEmpPrint,"Device ID=%u Not included.",device_ID);
		DebugPrint_uart_LineFeed(TEmpPrint,200);
		return 2;
	}
	else if((Device_BUFF_T[0]==device_ID)&&(Device_BUFF_T[1]==0x00))
	{//成功读取MAC
		for(uint8_t i=0;i<8;i++)//写入MAC
		{
			uint8TransToHEXchar(Device_BUFF_T[i+2],&DeviceList_t[device_ID-1].DeviceSN[i*2],1);
			//DeviceList_t[device_ID-1].DeviceSN[i]=Device_BUFF_T[i+2];
		}
		DeviceList_t[device_ID-1].DeviceID=device_ID;
		DeviceOnline_Falg[device_ID-1]=1;
		//UART_Send(&huart2,DeviceList_t[device_ID-1].DeviceSN,16,200);//debug使用		
		return 0;
	}
	else
	{//其他错误
		DebugPrint_uart_LineFeed("Warning!",200);
		DeviceOnline_Falg[device_ID-1]=0;
		return 3;
	}
	DebugPrint_uart_LineFeed("Err.",200);
	Clear_Buff(Device_BUFF_T,50,RecvBUFF_cc2530_MAX);
	Clear_Buff(Recv_BUFF_cc2530,50,RecvBUFF_cc2530_MAX);
	Clear_Buff(Rx_Buff3,100,BuffMAX3);	
}

uint8_t ReadAllSensorFlag=0;
uint8_t ZGFlag[6][8]={0};
/**
 * @brief  读取全部的传感器信息
 * @param	void
 * @retval	void
 * @author ZCD
 * @Time 2021年12月4日
*/
uint8_t ZigbeeRead_ALL_Sensor(void)
{
	// uint8_t UnconCnt_t=0;
	// for(uint8_t i=1;i<=DeviceNUM_MAX;i++)
	// {
	// 	for(uint8_t j=1;j<=5;j++)
	// 	{
	// 		uint8_t readRet_t=0;
	// 		Delay(100);
	// 		readRet_t=zigbee_Read_Sensor(i,j,DeviceSNList);
	// 		if(readRet_t==1)
	// 		{//超时退出
	// 			UnconCnt_t++;
	// 		}
	// 		else if(readRet_t!=2)
	// 		{
	// 			if((DeviceSNList[i].RH==0)&&(j==0x04))
	// 			{
	// 				j--;
	// 			}			
	// 			if((DeviceSNList[i].CO2==0)&&(j==0x01))
	// 			{
	// 				j--;
	// 			}								
	// 		}

	// 	}
	// 	if(UnconCnt_t==5)
	// 	{//从机离线
	// 		DeviceOnline_Falg[i-1]=0;
	// 		DebugPrint_uart_LineFeed("One Device Offline.",200);
	// 	}
	// }
	/*
	
	if(DeviceOnline_Falg[0]!=0)//device-1
	{
		zigbee_Read_Sensor(1,1,DeviceSNList);
		Delay(500);
		zigbee_Read_Sensor(1,2,DeviceSNList);
		Delay(500);
		zigbee_Read_Sensor(1,3,DeviceSNList);
		Delay(500);
		zigbee_Read_Sensor(1,4,DeviceSNList);
		Delay(500);
		zigbee_Read_Sensor(1,5,DeviceSNList);
		Delay(500);
	}
	if(DeviceOnline_Falg[1]!=0)//device-2
	{
		zigbee_Read_Sensor(2,1,DeviceSNList);
		Delay(500);
		zigbee_Read_Sensor(2,2,DeviceSNList);
		Delay(500);
		zigbee_Read_Sensor(2,3,DeviceSNList);
		Delay(500);
		zigbee_Read_Sensor(2,4,DeviceSNList);
		Delay(500);
		zigbee_Read_Sensor(2,5,DeviceSNList);
		Delay(500);
	}
	if(DeviceOnline_Falg[2]!=0)//device-3
	{
		zigbee_Read_Sensor(3,1,DeviceSNList);
		Delay(500);
		zigbee_Read_Sensor(3,2,DeviceSNList);
		Delay(500);
		zigbee_Read_Sensor(3,3,DeviceSNList);
		Delay(500);
		zigbee_Read_Sensor(3,4,DeviceSNList);
		Delay(500);
		zigbee_Read_Sensor(3,5,DeviceSNList);
		Delay(500);
	}
	if(DeviceOnline_Falg[3]!=0)//device-4
	{
		zigbee_Read_Sensor(4,1,DeviceSNList);
		Delay(500);
		zigbee_Read_Sensor(4,2,DeviceSNList);
		Delay(500);
		zigbee_Read_Sensor(4,3,DeviceSNList);
		Delay(500);
		zigbee_Read_Sensor(4,4,DeviceSNList);
		Delay(500);
		zigbee_Read_Sensor(4,5,DeviceSNList);
		Delay(500);
	}
	if(DeviceOnline_Falg[4]!=0)//device-5
	{
		zigbee_Read_Sensor(5,1,DeviceSNList);
		Delay(500);
		zigbee_Read_Sensor(5,2,DeviceSNList);
		Delay(500);
		zigbee_Read_Sensor(5,3,DeviceSNList);
		Delay(500);
		zigbee_Read_Sensor(5,4,DeviceSNList);
		Delay(500);
		zigbee_Read_Sensor(5,5,DeviceSNList);
		Delay(500);
	}
	if(DeviceOnline_Falg[5]!=0)//device-6
	{
		zigbee_Read_Sensor(6,1,DeviceSNList);
		Delay(500);
		zigbee_Read_Sensor(6,2,DeviceSNList);
		Delay(500);
		zigbee_Read_Sensor(6,3,DeviceSNList);
		Delay(500);
		zigbee_Read_Sensor(6,4,DeviceSNList);
		Delay(500);
		zigbee_Read_Sensor(6,5,DeviceSNList);
		//Delay(100);
	}			
	
*/
	ReadAllSensorFlag=1;
	uint8_t RetrtCnt_t=0;
	for(uint8_t i=1;i<=DeviceNUM_MAX;i++)
	{

		if(DeviceOnline_Falg[i-1]==1)
		{
			char ReadSensorDisplayBuff1[70]={0};
			sprintf(ReadSensorDisplayBuff1,"Reading sensor data...\tDevice ID:%u",i);
			DebugPrint_uart_LineFeed(ReadSensorDisplayBuff1,200);		
			uint8_t SensorIDFlag=0;
	 		for(uint8_t j=1;j<=5;j++)
	 		{
				if(SensorIDFlag!=j)
				{
					SensorIDFlag=j;
					char ReadSensorDisplayBuff2[70]={0};
					sprintf(ReadSensorDisplayBuff2,"Reading sensor ID:%u",j);
					DebugPrint_uart_LineFeed(ReadSensorDisplayBuff2,200);						
				}


				ReadAllSensorFlag=1;
				zigbee_SendCMD_IT(i,j);	
				ZGFlag[i-1][j-1]=0;
				UART3_Refresh_Flag=0;
				Delay(200);
				if(!Wait_Until_FlagTrue(&UART3_Refresh_Flag,5000))
				{//串口响应超时（5s）
					ReadAllSensorFlag=0;
					sprintf(ReadSensorDisplayBuff1,"Sensor ID: %u UART timeout.",j);
					DebugPrint_uart_LineFeed(ReadSensorDisplayBuff1,200);
					continue;
			  }
				uint16_t RdSen_Overtime_t=0;
				if(j==3)
				{
					RdSen_Overtime_t=1000;
				}
				else
				{ RdSen_Overtime_t=200;	}
				if(!Wait_Until_FlagTrue(&ZGFlag[i-1][j-1],RdSen_Overtime_t))
				{//数据读取标志未置位
					if(RetrtCnt_t<20)
					{
						RetrtCnt_t++;
						j--;
					}
					else
					{
						sprintf(ReadSensorDisplayBuff1,"Sensor ID: %u read timeout.",j);
						DebugPrint_uart(ReadSensorDisplayBuff1,200);
					}

				}
				ReadAllSensorFlag=0;
				//Delay(200);
			}
		}
		RetrtCnt_t=0;
	}
	
	return 0;
}
/**
 * @brief  发送后清空结构体数据
 * @param	void
 * @retval	void
 * @author ZCD
 * @Time 2021年12月4日
*/
uint8_t zigbeeClear_StructData(uint8_t Device_ID_t)
{
	DeviceSNList[Device_ID_t-1].CO2=0;
	DeviceSNList[Device_ID_t-1].Lux=0;
	DeviceSNList[Device_ID_t-1].NH3=0;
	DeviceSNList[Device_ID_t-1].RH=0;
	DeviceSNList[Device_ID_t-1].Temperture=0;
	return 0;
}

/**
 * @brief 传感器供电控制
 * @param	uint8_t Device_ID_t,uint8_t PowerCtrl_code 0-power off 1-power on
 * @retval uint8_t 
 * @author ZCD1300 
 * @Time 2022年4月17日
*/
uint8_t zigbee_Sensor_PowerCtrl(uint8_t Device_ID_t,uint8_t PowerCtrl_code)
{
	if(PowerCtrl_code!=0)
	{
		if( zigbee_ReadData(Device_ID_t,0x06))
		{
			//device离线
			DebugPrint_uart_LineFeed("Timeout -PowerON.",200);
			return 1;
		}		
		if(Device_BUFF_T[1]!=0xf6)//0xf6代表供电出错
		{
			if((Device_BUFF_T[0]==Device_ID_t)&&(Device_BUFF_T[1]==0x06))
			{//供电成功
				DeviceOnline_Falg[Device_ID_t-1]=1;
				return 0;
			}
		}
		else
		{return 1;}
	}
	else
	{
		if( zigbee_ReadData(Device_ID_t,0x07))
		{
			//device离线
			DebugPrint_uart_LineFeed("Timeout -PowerOFF.",200);
			return 1;
		}	
		if(Device_BUFF_T[1]!=0xf7)//0xf7断电出错
		{
			if((Device_BUFF_T[0]==Device_ID_t)&&(Device_BUFF_T[1]==0x07))
			{//断电成功
				DeviceOnline_Falg[Device_ID_t-1]=1;
				return 0;
			}
		}	
		else
		{return 1;}
	}
}
 /**
 * @brief  发送指令数据IT
 * @param	char uint8_t Device_num,uint8_t Sensor_type;目标设备和目标传感器
 * @retval	0-数据正常；1-数据超时；2-数据格式错误
 * @author ZCD
 * @Time 2022年4月18日
*/
int8_t zigbee_SendCMD_IT(uint8_t Device_num,uint8_t Sensor_type)
{
	char Send_Buff_t[6]={CMD_Head1_cc2530,CMD_Head2_cc2530,Device_num,Sensor_type,CMD_End1_cc2530,CMD_End2_cc2530};
	Uart_Send_cc2530(Send_Buff_t,6);	
	return 0;
}

uint8_t ZgHeadOffset=0;
 /**
 * @brief  接收数据中断处理函数
 * @param	char Send_temp,uint16_t Send_len_t;发送内容和数据长度
 * @retval	0-数据正常；1-数据超时；2-数据格式错误
 * @author ZCD
 * @Time 2022年4月18日
*/
int8_t zigbee_SendReceive_ITCallback(void)
{
	char head_t[2]={CMD_End1_cc2530,CMD_End2_cc2530};

	ZgHeadOffset=0;
	if(Rx_Buff3[0]!=CMD_Head1_cc2530)
	{
		ZgHeadOffset=1;
	}
	if((Rx_Buff3[0+ZgHeadOffset]==CMD_Head1_cc2530)&&(Rx_Buff3[1+ZgHeadOffset]==CMD_Head2_cc2530))
	{//头检测
		if(StringSearch_Assign(head_t,Rx_Buff3,2,50))
		{//尾检测
			Rx_Buff3[StringSecanPointer_Assign_Buff]=0;
			Rx_Buff3[StringSecanPointer_Assign_Buff+1]=0;
			Rx_Buff3[0+ZgHeadOffset]=0;
			Rx_Buff3[1+ZgHeadOffset]=0;			
			switch (Rx_Buff3[2+ZgHeadOffset])
			{//device ID
				case 0x00:
				{
					/*  */
					zigbee_SendReceive_CMD_IT(DeviceSNList,0);
					break;
				}
				case 0x01:
				{
					/*  */
					zigbee_SendReceive_CMD_IT(DeviceSNList,1);
					break;
				}
				case 0x02:
				{
					/*  */
					zigbee_SendReceive_CMD_IT(DeviceSNList,2);
					break;
				}
				case 0x03:
				{
					/*  */
					zigbee_SendReceive_CMD_IT(DeviceSNList,3);
					break;
				}
				case 0x04:
				{
					/*  */
					zigbee_SendReceive_CMD_IT(DeviceSNList,4);
					break;
				}		
				case 0x05:
				{
					/*  */
					zigbee_SendReceive_CMD_IT(DeviceSNList,5);
					break;
				}	
				case 0x06:
				{
					/*  */
					zigbee_SendReceive_CMD_IT(DeviceSNList,6);
					break;
				}																		
			
			default:
			{break;}
			}
			Rx_Buff3[2+ZgHeadOffset]=0;
		}
	}
	else
	{

	}
}

 /**
 * @brief  接收中断的CMD code 处理
 * @param	
 * @retval	0-数据正常；1-数据超时；2-数据格式错误
 * @author ZCD
 * @Time 2022年4月18日
*/
int8_t zigbee_SendReceive_CMD_IT(Device_t *DeviceList_t,uint8_t device_ID)
{
	switch (Rx_Buff3[3+ZgHeadOffset])
	{
		case 0x00:
		{//mac
			/* code */
			if(Rx_Buff3[2+ZgHeadOffset]!=0x00)
			{
				for(uint8_t i=0;i<8;i++)
				{
					uint8TransToHEXchar(Rx_Buff3[i+4+ZgHeadOffset],&DeviceList_t[device_ID-1].DeviceSN[i*2],1);				
				}			
			}
			DebugPrint_uart("OK",200);
			break;		
		}		
		case 0x01:
		{//CO2
			/* code */
			CO2_D=(uint32_t)Rx_Buff3[2+4+ZgHeadOffset]<<8|(uint32_t)Rx_Buff3[2+5+ZgHeadOffset];
			DeviceList_t[device_ID-1].CO2=CO2_D;
			if(DeviceList_t[device_ID-1].CO2>4500)
			{
				DeviceList_t[device_ID-1].CO2=0;
			}			
			ZGFlag[device_ID-1][0]=1;
			DebugPrint_uart("OK",200);
			break;		
		}		
		case 0x02:
		{//NH3
			/* code */
			uint32_t nh3_t=0;
			nh3_t=(uint32_t)Rx_Buff3[4+2+ZgHeadOffset]<<8|(uint32_t)Rx_Buff3[5+2+ZgHeadOffset];	
			NH3_D=nh3_t/100;	
			DeviceList_t[device_ID-1].NH3=NH3_D;
			if(DeviceList_t[device_ID-1].NH3>349)
			{
				DeviceList_t[device_ID-1].NH3=0;
			}
			ZGFlag[device_ID-1][1]=1;
			DebugPrint_uart("OK",200);
			break;		
		}		
		case 0x03:
		{//Lux
			/* code */
			uint32_t lux_t=0;
			lux_t=(uint32_t)Rx_Buff3[4+2+ZgHeadOffset]<<8|(uint32_t)Rx_Buff3[5+2+ZgHeadOffset];	
			Lux_D=((float)lux_t*10)/12;
			DeviceList_t[device_ID-1].Lux=Lux_D;
			ZGFlag[device_ID-1][2]=1;
			DebugPrint_uart("OK",200);
			break;		
		}		
		case 0x04:
		{//RH
			/* code */
			//uint32_t  rh_t1=0;
			rh_t=0;
			rh_t=(rh_t|Rx_Buff3[3+2+ZgHeadOffset])<<8;
			rh_t=(rh_t|Rx_Buff3[4+2+ZgHeadOffset])<<8;
			rh_t=rh_t|Rx_Buff3[5+2+ZgHeadOffset];
			rh_t=rh_t>>4;
			RH_D=(((double)rh_t/1048576.0))*100;
			DeviceList_t[device_ID-1].RH=RH_D;
			ZGFlag[device_ID-1][3]=1;
			DebugPrint_uart("OK",200);
			break;		
		}		
		case 0x05:
		{//Temper
			/* code */
			temper_t=0;
			temper_t=(temper_t|Rx_Buff3[3+2+ZgHeadOffset])<<8;
			temper_t=(temper_t|Rx_Buff3[4+2+ZgHeadOffset])<<8;
			temper_t=(temper_t|Rx_Buff3[5+2+ZgHeadOffset]);					
			temper_t=temper_t&0x0fffff;			
			Temperture_D=(((double)temper_t/1048576.0)*200)-50;
			DeviceList_t[device_ID-1].Temperture=	Temperture_D;
			ZGFlag[device_ID-1][4]=1;
			DebugPrint_uart("OK",200);
			break;		
		}		
		case 0x06:
		{//PowerON
			/* code */
			break;		
		}		
		case 0x07:
		{//PowerOFF
			/* code */
			break;		
		}		
		case 0x08:
		{//Power?
			/* code */
			break;		
		}		
		case 0x09:
		{//Key
			/* code */
			break;		
		}		
		case 0xff:
		{
			/* code */
			break;		
		}
		case 0xfe:
		{
			/* code */
			break;		
		}
		case 0xf1:
		{
			/* code */
			break;		
		}
		case 0xf2:
		{
			/* code */
			break;		
		}
		case 0xf3:
		{//光照传感器错误
			/* code */
		//	DeviceList_t[device_ID-1].Lux=9999;
			ZGFlag[device_ID-1][2]=1;
			DebugPrint_uart("Offline",200);
			break;		
		}
		case 0xf4:
		{//湿度传感器错误
			/* code */
			//DeviceList_t[device_ID-1].RH=100;
			ZGFlag[device_ID-1][3]=1;
			DebugPrint_uart("Offline",200);
			break;		
		}
		case 0xf5:
		{//温度传感器错误
			/* code */
			//DeviceList_t[device_ID-1].Temperture=	99.9;
			ZGFlag[device_ID-1][4]=1;
			DebugPrint_uart("Offline",200);
			break;		
		}
		case 0xf6:
		{
			/* code */
			break;		
		}
		case 0xf7:
		{
			/* code */
			break;		
		}
		case 0xf8:
		{
			/* code */
			break;		
		}
		default:
		{break;}
	}
	Rx_Buff3[3+ZgHeadOffset]=0;
}


